﻿using ADAMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ADAMVC.Controllers
{
    public class TelevisionController : Controller
    {
        //
        // GET: /Television/
        UsersContext u = new UsersContext();
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Television()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Television(ADAMVC.Models.Television b)
        {
            ADAMVC.Models.UsersContext uc = new Models.UsersContext();
            uc.televisions.Add(b);
            uc.SaveChanges();
            return View();
        }
        public ActionResult televisionlist(string username)
        {
            var q1 = (from t2 in u.televisions
                      join s in u.addproducts
                                   on t2.ProductName equals s.ProductName
                      where s.UserName == username
                      select new { t2.ProductName, t2.AddDuration, t2.Duration, t2.Repetition, t2.channel ,t2.ProductBrand,t2.UserName});
            var televisions = new List<Television>();
            foreach (var p in q1)
            {
                televisions.Add(new Television()
                {
                    ProductName = p.ProductName,
                    AddDuration = p.AddDuration,
                    Duration = p.Duration,
                    Repetition = p.Repetition,
                    channel = p.channel,
                   ProductBrand=p.ProductBrand,
                   UserName=p.UserName
                });


            }
            return View(televisions);
        }
        

    }
}

