﻿using ADAMVC.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;

namespace ADAMVC.Controllers
{
    public class HomeController : Controller
    {
        UsersContext u = new UsersContext();
        public ActionResult Index()
        {
            ImageDisplay imgdis = new ImageDisplay();

            return View(imgdis);
        }

        public ActionResult About()
        {
            ImageDisplay img = new ImageDisplay();
            return View(img);
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        

        public ActionResult Media()
        {
            return View();
        }
        public ActionResult payment()
        {
            return View();
        }
      [HttpGet]
        public ActionResult credit()
        {
            return View();
        }
         [HttpPost]
        public ActionResult credit(credit c )
      {
          return RedirectToAction("thanku");
      }
         [HttpGet]
        public ActionResult debit()
        {
            return View();
        }
         [HttpPost]
         public ActionResult debit(debit c)
         {
             return RedirectToAction("thanku");
         }
        public ActionResult thanku()
        {
            return View();
        }



        public ActionResult adminproductlist()
        {
            List<AddProduct> product = u.addproducts.ToList();
            return View(product);
        }

        public ActionResult adminbannerlist()
        {
            List<banner> Banner = u.banners.ToList();
            return View(Banner);
        }
        public ActionResult admintelevisionlist()
        {
            List<Television> television = u.televisions.ToList();
            return View(television);
        }
    }
}


    