﻿using ADAMVC.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebMatrix.WebData;

namespace ADAMVC.Controllers
{
    public class addproductController : Controller
    {
        //
        // GET: /addproduct/
        UsersContext u = new UsersContext();
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult list(string username)
        {
           
                var q = (from t in u.UserProfiles
                         join sc in u.addproducts
                                      on t.UserName equals sc.UserName
                         where sc.UserName == WebSecurity.CurrentUserName
                         select new { sc.productId, sc.ProductName, sc.ProductBrand, sc.ProductFeatures, sc.Warranty, sc.price, sc.UserName });
                var addproducts = new List<AddProduct>();
                foreach (var p in q)
                {
                    addproducts.Add(new AddProduct()
                    {
                        productId = p.productId,
                        ProductName = p.ProductName,
                        ProductBrand = p.ProductBrand,
                        ProductFeatures = p.ProductFeatures,
                        Warranty = p.Warranty,
                        price = p.price,
                        UserName = p.UserName


                    });

                }
                return View(addproducts);
            }
          

       // }
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(ADAMVC.Models.AddProduct ap)
        {
            if (ModelState.IsValid)
            {
                ADAMVC.Models.UsersContext uc = new Models.UsersContext();
                uc.addproducts.Add(ap);
                uc.SaveChanges();
                return RedirectToAction("list", new { username = ap.UserName });
            }

            return View();
        }
        [HttpGet]
        // [ActionName("Edit")]
        public ActionResult Edit(AddProduct p)
        {
          
            return View(p);
        }

        [HttpPost]
        // [ActionName("Edit")]
        public ActionResult Edit(AddProduct p, int id)
        {
            AddProduct ac = u.addproducts.Find(id);
            ac.ProductName = p.ProductName;
            ac.ProductBrand = p.ProductBrand;
            ac.ProductFeatures = p.ProductFeatures;
            ac.Warranty = p.Warranty;
            ac.price = p.price;
            u.Entry(ac).State = EntityState.Modified;
            u.SaveChanges();
            return RedirectToAction("list", new { username = ac.UserName});
            //return View();
        }
        [HttpGet]
        public ActionResult Details(int id=0)
        {
            AddProduct ac = u.addproducts.Find(id);
            if(ac==null)
            {
                return HttpNotFound();
            }
            return View(ac);
        }
        [HttpGet]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AddProduct ad = u.addproducts.Find(id);
            if (ad == null)
            {
                return HttpNotFound();
            }
            return View(ad);
           // return RedirectToAction("list");
        }

        // POST: PersonalDetails/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            AddProduct ad = u.addproducts.Find(id);
            u.addproducts.Remove(ad);
            u.SaveChanges();
            return RedirectToAction("list");
        }
       }
}
