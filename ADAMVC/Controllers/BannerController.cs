﻿using ADAMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMatrix.WebData;



namespace ADAMVC.Controllers
{
    public class BannerController : Controller
    {
        //
        // GET: /Banner/
        UsersContext u = new UsersContext();
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Banner()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Banner(ADAMVC.Models.banner b)
        {
            ADAMVC.Models.UsersContext uc = new Models.UsersContext();
            uc.banners.Add(b);
            uc.SaveChanges();
            return RedirectToAction("bannerlist", new { username = b.UserName });
        }
        public ActionResult bannerlist(string username)
        {
          
                
             var banners = new List<banner>();

               
            var q1 = (from t1 in u.banners
                      join s in u.addproducts
                                   on t1.ProductName equals s.ProductName
                      where s.UserName == WebSecurity.CurrentUserName

                      select new { t1.ProductName,t1.ProductBrand,t1.Content ,t1.size,t1.quantity,t1.printedside,t1.color,t1.BannerType});
        
            foreach (var p in q1)
            {
                banners.Add(new banner()
                {
                    ProductName = p.ProductName,
                  ProductBrand=p.ProductBrand,
                    Content = p.Content,
                    size=p.size,
                    quantity=p.quantity,
                    printedside=p.printedside,
                    color=p.color,
                   BannerType=p.BannerType
                });


            }
            return View(banners);
        }
       
        public ActionResult imageUpload(HttpPostedFileBase file, string Username)
        {

            if (file != null)
            {
                UsersContext db = new UsersContext();
                string ImageName = System.IO.Path.GetFileName(file.FileName);
                string physicalPath = Server.MapPath("~/Images/" + ImageName);

                // save image in folder
                file.SaveAs(physicalPath);

                //save new record in database
                imageupload1 newRecord = new imageupload1();
                newRecord.UserId = User.Identity.Name;
                newRecord.brandname = Request.Form["brandname"];
                newRecord.UserName = Request.Form["UserName"];
                newRecord.imageurl = ImageName;
                db.imageuploads.Add(newRecord);
                db.SaveChanges();
            
                
        }     //Display records
                return RedirectToAction("../Home/Payment/");
    }




        public ActionResult Display()
        {
            return View(u.imageuploads.Where(x=>x.UserId==User.Identity.Name).ToList());
        }
                [HttpGet]
        public ActionResult Account()
        {
            return View();
        }
      
               
        

    }
}
