﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ADAMVC.Models
{
         [Table("Addproduct")]
    public class AddProduct
    {
   
        
            [Key]

            public int productId { get; set; }
              [Required]
            public string ProductName { get; set; }
              [Required]
            public string ProductBrand { set; get; }
              [Required]
            public string ProductFeatures { set; get; }
              [Required]
            public string Warranty { set; get; }
              [Required]
            public int price { set; get; }
              [Required]
            public string UserName { set; get; }

        }
    }
