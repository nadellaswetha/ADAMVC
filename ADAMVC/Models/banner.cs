﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ADAMVC.Models
{
    [Table("Banner")]
    public class banner
    {
        [Key]
        public int id { get; set; }
         [Required]
        public string UserName { set; get; }
         [Required]
        public string ProductName { get; set; }
         [Required]
        public string ProductBrand { set; get; }
         [Required]
        public string Content { set; get; }
         [Required]
        public string size { set; get; }
         [Required]
        public int quantity { set; get; }
         [Required]
        public string printedside { set; get; }
   
        
        public string color { set; get; }
         [Required]
        public string BannerType { set; get; }
    }
   
}