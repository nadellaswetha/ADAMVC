﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ADAMVC.Models
{
    [Table("Television")]
    public class Television
    {
        [Key]
        public int id { get; set; }
         [Required]
        public string ProductName { set; get; }
         [Required]
        public string AddDuration { set; get; }
         [Required]
        public int Duration
        {
            set;
            get;
        }
         [Required]
        public string Repetition { set; get; }
         [Required]
        public string channel { set; get; }
         [Required]
        public string ProductBrand { set; get; }
         [Required]
        public string UserName { set; get; }
    }
}